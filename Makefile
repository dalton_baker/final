PROJECT_NAME = final
SOURCE = dash.cpp inputProcessor.cpp tools.cpp fileProcessor.cpp commandLineInterface.cpp pidManager.cpp memoryManager.cpp

OBJS = $(SOURCE:.cpp=.o)

#GNU C/C++ Compiler
GCC = g++
LINK = g++

# Compiler flags
CFLAGS = -Wall -O3 -std=c++11
CXXFLAGS = $(CFLAGS)

# Fill in special libraries needed here
LIBS = -lm -lpthread

.PHONY: clean

all : dash

dash: $(OBJS)
	$(LINK) -o $@ $^ $(LIBS)

clean:
	rm -rf *.o *.d core dash $(PROJECT_NAME).tgz

debug: CXXFLAGS = $(CFLAGS) -DDEBUG -g
debug: dash

tar: clean
	tar zcvf $(PROJECT_NAME).tgz ../$(PROJECT_NAME)/Makefile ../$(PROJECT_NAME)/*.cpp ../$(PROJECT_NAME)/*.h ../$(PROJECT_NAME)/*.pdf

-include $(SOURCE:.cpp=.d)

%.d: %.cpp
	@set -e; /bin/rm -rf $@;$(GCC) -MM $< $(CXXFLAGS) > $@
