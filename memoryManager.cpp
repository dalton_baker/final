/**************************************************************************/ /**
 * @file
 * @brief This file contains the members of the MemoryManager class.
 *****************************************************************************/
#include "memoryManager.h"

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * The constructor MemoryManager
 ******************************************************************************/
MemoryManager::MemoryManager()
{
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This is where the Memory Manager starts
 * 
 * @param[in] inputList - the arguments provided to the program
 ******************************************************************************/
void MemoryManager::start(std::list<std::string> inputList)
{
    if (inputList.size() != 2 || !isInt(inputList.back()))
    {
        std::cout << "  malformed command:\n    must be \"advmemman <num processes>\" <num processes> must be an int\n";
        return;
    }

    initialise(stoi(inputList.back()));
    runSimulation();
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will clear out all existing variables and initialize them with new 
 * values
 * 
 * @param[in] numOfProcesses - the number of processes to initialize
 ******************************************************************************/
void MemoryManager::initialise(int numOfProcesses)
{
    numProcesses = numOfProcesses;

    processes.clear();
    logicalMemory.clear();
    pageTable.clear();
    physicalMemory.clear();
    physicalMemoryOrder.clear();
    hardDrive.clear();
    for (int i = 0; i < numProcesses; i++)
    {
        Process newProcess;
        newProcess.sizeInBytes = (rand() % 4 + 1) * 1024;
        int memoryToAllocate = newProcess.sizeInBytes;

        if (!logicalMemory.empty() && logicalMemory.back().usedMemory < BYTES_PER_FRAME)
        {
            Page pagetoAddTo = logicalMemory.back();
            memoryToAllocate -= (BYTES_PER_FRAME - pagetoAddTo.usedMemory);
            newProcess.pageTable.push_back(pagetoAddTo);
        }

        while (memoryToAllocate > 0)
        {
            Page page;
            page.pageNum = logicalMemory.size();

            if (memoryToAllocate >= BYTES_PER_FRAME)
            {
                page.usedMemory = BYTES_PER_FRAME;
                memoryToAllocate -= BYTES_PER_FRAME;
            }
            else
            {
                page.usedMemory += memoryToAllocate;
                memoryToAllocate = 0;
            }

            newProcess.pageTable.push_back(page);
            logicalMemory.push_back(page);
            hardDrive.push_back(page);
            pageTable[page.pageNum] = PageTableEntry();
        }

        processes.push_back(newProcess);
    }

    for (int i = 0; i < NUM_FRAMES; i++)
    {
        physicalMemory[i].empty = true;
    }
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will run the simulation
 ******************************************************************************/
void MemoryManager::runSimulation()
{
    std::string input;

    do
    {
        int processIdToLoad = rand() % processes.size();
        Process processToLoad = processes[processIdToLoad];

        std::cout << "\n\nAccessing process " << processIdToLoad << " containing pages ";

        for (Page pg : processToLoad.pageTable)
        {
            checkPage(pg.pageNum);

            if (pg.pageNum < 26)
            {
                std::cout << pageNames[pg.pageNum] << " ";
            }
            else
            {
                std::cout << pageNames[pg.pageNum/ 26 - 1] << pageNames[pg.pageNum % 26] << " ";
            }
        }

        std::cout << std::endl;

        printCurrentState();

        std::cout << "<Enter> to continue \'quit\' to quit: ";
        std::getline(std::cin, input);

    } while (input != "quit");
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This function will check if the required page is currently loaded into memory
 * It will ensure the required page is loaded into memory.
 * 
 * @param[in] pageNum - The id of the page to load in
 ******************************************************************************/
void MemoryManager::checkPage(int pageNum)
{
    if (pageTable[pageNum].valid)
    {
        return;
    }

    int frameNum = handleFault(pageNum);

    if (frameNum >= 0)
    {
        pageTable[pageNum].frame = frameNum;
        pageTable[pageNum].valid = true;
    }

    checkPage(pageNum);
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This function will handle a fault. If there is free space in the memory,
 * the requested page will just load in.
 * 
 * @param[in] pageNum - The id of the page to load in
 * @return - the index of the frame the page was loaded into
 ******************************************************************************/
int MemoryManager::handleFault(int pageNum)
{
    Page page;
    for (Page p : hardDrive)
    {
        if (p.pageNum == pageNum)
        {
            page = p;
        }
    }

    for (int i = 0; i < NUM_FRAMES; i++)
    {
        if (physicalMemory[i].empty)
        {
            physicalMemory[i].page = page;
            physicalMemory[i].empty = false;
            physicalMemoryOrder.push_back(i);
            hardDrive.remove(page);
            return i;
        }
    }

    return replaceFrame(page);
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This function will swap a page from the HD to the memory.
 * It will also move the page in the frame to the HD.
 * 
 * @param[in] pageToLoad - page to load into memory
 * @return - the index of the memory location the page was loaded into
 ******************************************************************************/
int MemoryManager::replaceFrame(Page pageToLoad)
{
    int firstIn = physicalMemoryOrder.front();
    int pageToRemove = physicalMemory[firstIn].page.pageNum;

    physicalMemoryOrder.pop_front();
    physicalMemoryOrder.push_back(firstIn);

    hardDrive.push_back(physicalMemory[firstIn].page);
    physicalMemory[firstIn].page = pageToLoad;
    hardDrive.remove(pageToLoad);

    pageTable[pageToRemove].valid = false;

    return firstIn;
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * Prints the current state of the memory to the screen
 ******************************************************************************/
void MemoryManager::printCurrentState()
{
    std::cout << "\nLogical Memory      Page Table      Physical Memory";
    std::cout << "\n     ____            ____ ___             ____\n";

    int i = 0;
    for (Page p : logicalMemory)
    {
        //LogicalMemory table
        std::string pageNum;
        std::string index;
        if (p.pageNum < 26)
        {
            pageNum = " ";
            pageNum.push_back(pageNames[p.pageNum]);
        }
        else
        {
            pageNum.push_back(pageNames[p.pageNum / 26 - 1]);
            pageNum.push_back(pageNames[p.pageNum % 26]);
        }

        if (i < 10)
        {
            index = " " + std::to_string(i);
        }
        else
        {
            index = std::to_string(i);
        }
        std::cout << " " << index << " | " << pageNum << " |";

        //PageTable info
        PageTableEntry pageTableEntry = pageTable[i];
        std::string pageTableValid = pageTableEntry.valid ? "v" : "i";
        std::string frameNumber;
        if (!pageTableEntry.valid)
        {
            frameNumber = "  ";
        }
        else if (pageTableEntry.frame < 10)
        {
            frameNumber = " " + std::to_string(pageTableEntry.frame);
        }
        else
        {
            frameNumber = std::to_string(pageTableEntry.frame);
        }
        std::cout << "          | " << frameNumber << " | " << pageTableValid << " |";

        //PhysicalMemeory info
        if (i < (int)physicalMemory.size())
        {
            if (physicalMemory[i].empty)
            {
                pageNum = "  ";
            }
            else if (physicalMemory[i].page.pageNum < 26)
            {
                pageNum = " ";
                pageNum.push_back(pageNames[physicalMemory[i].page.pageNum]);
            }
            else
            {
                pageNum.clear();
                pageNum.push_back(pageNames[physicalMemory[i].page.pageNum / 26 - 1]);
                pageNum.push_back(pageNames[physicalMemory[i].page.pageNum % 26]);
            }

            std::cout << "        " << index << " | " << pageNum << " |";

            //cap off the bottom row
            std::cout << "\n    |____|          |____|___|           |____|\n";
        }
        else
        {
            //cap off the bottom row if there are no memory frames left
            std::cout << "\n    |____|          |____|___|\n";
        }

        i++;
    }

    for (int i = logicalMemory.size(); i < (int)physicalMemory.size(); i++)
    {
        std::string pageNum;
        std::string index;
        if (i < 10)
        {
            index = " " + std::to_string(i);
        }
        else
        {
            index = std::to_string(i);
        }

        if (physicalMemory[i].empty)
        {
            pageNum = "  ";
        }
        else if (physicalMemory[i].page.pageNum < 26)
        {
            pageNum = " ";
            pageNum.push_back(pageNames[physicalMemory[i].page.pageNum]);
        }
        else
        {
            pageNum.clear();
            pageNum.push_back(pageNames[physicalMemory[i].page.pageNum / 26 - 1]);
            pageNum.push_back(pageNames[physicalMemory[i].page.pageNum % 26]);
        }

        std::cout << "                                      " << index << " | " << pageNum << " |";

        //cap off the bottom row
        std::cout << "\n                                         |____|\n";
    }

    std::cout << "\nPages in Hard Drive\n     ";

    for (Page p : hardDrive)
    {
        if (p.pageNum < 26)
        {
            std::cout << pageNames[p.pageNum] << " ";
        }
        else
        {
            std::cout << pageNames[p.pageNum / 26 - 1] << pageNames[p.pageNum % 26] << " ";
        }
    }

    std::cout << std::endl;
}
