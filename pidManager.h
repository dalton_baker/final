/**************************************************************************//**
 * @file
 * @brief This is the header file for pidManager.cpp
 *****************************************************************************/
#ifndef __PIDMANAGER_H
#define __PIDMANAGER_H

#include <pthread.h>

#define MIN_PID 300
#define MAX_PID 500

/*!
* @brief This holds the Pid Manager
*/
class PidManager
{
public:
    PidManager();
    int allocateMap();
    int allocatePid();
    void releasePid(int);

private: 
    bool mapAllocated = false;
    bool pidMap[MAX_PID+1];
    pthread_mutex_t lock;

};

#endif
