/**************************************************************************/ /**
 * @file
 * @brief This is the header file for memoryManager.cpp
 *****************************************************************************/
#ifndef __MEMORYMANAGER_H
#define __MEMORYMANAGER_H

#include <list>
#include <string>
#include <iostream>
#include <map>
#include <vector>

#include "tools.h"

#define NUM_FRAMES 8         //f
#define BYTES_PER_FRAME 2048 //b

struct PageTableEntry
{
    int frame = -1;
    bool valid = false;
};

struct Page
{
    int pageNum = -1;
    int usedMemory = 0;
    
    bool operator==(const Page &other) const
    {
        return (pageNum == other.pageNum && usedMemory == other.usedMemory);
    }
};

struct Frame
{
    Page page;
    bool empty = true;
};

struct Process
{
    int sizeInBytes;
    std::list<Page> pageTable;
};

/*!
* @brief This holds the Memory Manager
*/
class MemoryManager
{
private:
    int numProcesses; //q
    std::vector<Process> processes;
    std::list<Page> hardDrive;
    std::list<Page> logicalMemory;
    std::map<int, PageTableEntry> pageTable;
    std::map<int, Frame> physicalMemory;
    std::list<int> physicalMemoryOrder;
    const char pageNames[26] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                                'U', 'V', 'W', 'X', 'Y', 'Z'};

public:
    MemoryManager();
    void start(std::list<std::string>);

private:
    void initialise(int);
    void runSimulation();
    void checkPage(int);
    int handleFault(int);
    int replaceFrame(Page);
    void printCurrentState();
};

#endif
