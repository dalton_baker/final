/**************************************************************************//**
 * @file
 * @brief This is the header file for commandLineInterface.cpp
 *****************************************************************************/
#ifndef __COMMANDLINEINTERFACE_H
#define __COMMANDLINEINTERFACE_H

#include "tools.h"

#include <iostream>
#include <algorithm>
#include <string>
#include <list>

#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/resource.h>

/*!
* @brief This handles all command line calls using fork and exec()
*/
class CommandLineInterface
{
    //FileProcessor fileProcessor;

public:
    CommandLineInterface();
    void processCommand(std::list<std::string>);
    void processRedirection(std::list<std::string>);
    void processPipe(std::list<std::string>);

private:
};

#endif
