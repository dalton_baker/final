/**************************************************************************/ /**
 * @file
 * @brief This file contains the members of the PidManager class.
 *****************************************************************************/
#include "pidManager.h"

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * The constructor PidManager
 ******************************************************************************/
PidManager::PidManager()
{
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This function initiallizes a map of pid values
 * 
 * @return - 0 if map was allocated, 1 if not.
 ******************************************************************************/
int PidManager::allocateMap()
{
    if(mapAllocated)
    {
        return 0;
    }

    try
    {
        for (int i = MIN_PID; i <= MAX_PID; i++)
        {
            pidMap[i] = false;
        }
    }
    catch (...)
    {
        mapAllocated = false;
        return 1;
    }

    mapAllocated = true;
    return 0;
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This functions allocates a pid
 * 
 * @return - The alocated pid, or 1 if no pid was allocated
 ******************************************************************************/
int PidManager::allocatePid()
{
    int pid = MIN_PID;

    pthread_mutex_lock(&lock);
    while (pidMap[pid])
    {
        pid++;

        if (pid > MAX_PID)
        {
            return 1;
        }
    }

    pidMap[pid] = true;
    pthread_mutex_unlock(&lock);

    return pid;
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This function releases a pid
 * 
 * @param[in] pid - the pid to release
 ******************************************************************************/
void PidManager::releasePid(int pid)
{
    pthread_mutex_lock(&lock);
    pidMap[pid] = false;
    pthread_mutex_unlock(&lock);
}