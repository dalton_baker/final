/**************************************************************************/ /**
 * @file
 * @brief This file contains the members of the FileProcessor class.
 *****************************************************************************/
#include "fileProcessor.h"

/**************************************************************************/ /**
 * @author Dalton Baker
 *
 * @par Description:
 * The constructor for the FileProcessor class.
 *****************************************************************************/
FileProcessor::FileProcessor()
{
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will tell you if a given string represents a legitimate path to a folder
 * 
 * @param[in] folderPath - the path to the folder
 * @return - bool
 ******************************************************************************/
bool FileProcessor::isFolder(std::string folderPath)
{
    struct stat st;

    stat(folderPath.c_str(), &st);

    return S_ISDIR(st.st_mode);
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will tell you if a given string represents a legitimate path to a folder
 * 
 * @param[in] filePath - the path to the folder
 * @return - bool
 ******************************************************************************/
bool FileProcessor::isFile(std::string filePath)
{
    struct stat st;

    stat(filePath.c_str(), &st);

    return S_ISREG(st.st_mode);
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will get all file contents and return them as a string
 * 
 * @param[in] filePath - the path to the file
 * @return - list of strings containing the file contents
 ******************************************************************************/
std::list<std::string> FileProcessor::getFileContents(std::string filePath)
{
    std::list<std::string> returnList;

    std::ifstream myfile(filePath);

    std::string fileReader;
    while (std::getline(myfile, fileReader))
    {
        returnList.push_back(fileReader);
    }

    myfile.close();

    return returnList;
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will get the current working dirrectories path
 * 
 * @return - the path of the current directory
 ******************************************************************************/
std::string FileProcessor::getCurrentPath()
{
    char *currentPath = get_current_dir_name();
    std::string returnPath = currentPath;
    free(currentPath);
    return returnPath;
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will get all subdirrectories of a directory, will NOT return . or ..
 * 
 * @param[in] folderPath - the path to the folder
 * @return - the contents of the directory
 ******************************************************************************/
std::list<std::string> FileProcessor::getSubDirectories(std::string folderPath)
{
    std::list<std::string> folderContents;

    DIR *dir;
    struct dirent *dirEntry;

    if ((dir = opendir(folderPath.c_str())) == NULL)
    {
        return folderContents;
    }

    while ((dirEntry = readdir(dir)) != NULL)
    {
        if (dirEntry->d_type == DT_DIR || dirEntry->d_type == DT_LNK)
        {
            folderContents.push_back(dirEntry->d_name);
        }
    }

    folderContents.pop_front(); //get rid of "."
    folderContents.pop_front(); //get rid of ".."

    return folderContents;
}