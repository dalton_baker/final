/**************************************************************************/ /**
 * @file
 * @brief This is the header file for inputProcessor.cpp
 *****************************************************************************/
#ifndef __PROCESSINPUT_H
#define __PROCESSINPUT_H

#include "tools.h"
#include "fileProcessor.h"
#include "commandLineInterface.h"
#include "pidManager.h"
#include "memoryManager.h"

#include <iostream>
#include <string>
#include <list>
#include <unistd.h>

/*!
* @brief This will process input from the user
*/
class InputProcessor
{
    FileProcessor fileProcessor;
    PidManager pidManager;
    CommandLineInterface redirectionHandler;
    MemoryManager memoryManager;

public:
    InputProcessor();
    void processInput(std::string);

private:
    void processCmdnm(std::list<std::string>);
    void processPid(std::list<std::string>);
    void processSystat(std::list<std::string>);
    void processCd(std::list<std::string>);
    void memManager(std::list<std::string>);
    void processTestpid();

    friend void* ThreadEntryFunc(void *);
};

#endif
