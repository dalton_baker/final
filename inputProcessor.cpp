/**************************************************************************/ /**
 * @file
 * @brief This file contains the members of the ProcessInput class.
 *****************************************************************************/
#include "inputProcessor.h"

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * The constructor InputPorcessor
 ******************************************************************************/
InputProcessor::InputProcessor()
{
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process the users input
 * 
 * @param[in] userInput - the users input
 ******************************************************************************/
void InputProcessor::processInput(std::string userInput)
{
    std::list<std::string> splitInput = splitString(userInput, " ");

    if (splitInput.front() == "cmdnm")
    {
        processCmdnm(splitInput);
    }
    else if (splitInput.front() == "pid")
    {
        processPid(splitInput);
    }
    else if (splitInput.front() == "systat")
    {
        processSystat(splitInput);
    }
    else if (splitInput.front() == "cd")
    {
        processCd(splitInput);
    }
    else if (splitInput.front() == "testpid")
    {
        processTestpid();
    }
    else if (splitInput.front() == "memman")
    {
        memManager(splitInput);
    }
    else if (splitInput.front() == "advmemman")
    {
        memoryManager.start(splitInput);
    }
    else if (std::find(splitInput.begin(), splitInput.end(), "|") != splitInput.end())
    {
        redirectionHandler.processPipe(splitInput);
    }
    else if (std::find(splitInput.begin(), splitInput.end(), ">") != splitInput.end() ||
             std::find(splitInput.begin(), splitInput.end(), "<") != splitInput.end())
    {
        redirectionHandler.processRedirection(splitInput);
    }
    else
    {
        redirectionHandler.processCommand(splitInput);
    }
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process the "cmdnm" command
 * 
 * @param[in] inputList - A list of strings, the users input split by " "
 ******************************************************************************/
void InputProcessor::processCmdnm(std::list<std::string> inputList)
{
    if (inputList.size() != 2 || !isInt(inputList.back()))
    {
        std::cout << "  malformed command:\n    must be \"cmdnm <pid>\" <pid> must be an int\n";
        return;
    }

    if (!fileProcessor.isFolder("/proc/" + inputList.back()))
    {
        std::cout << "  pid does not exist: " << inputList.back() << "\n";
        return;
    }

    for (std::string s : fileProcessor.getFileContents("/proc/" + inputList.back() + "/status"))
    {
        if (s.find("Name:") != std::string::npos)
        {
            std::cout << splitString(s, "\t").back() << "\n";
            return;
        }
    }
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process the "pid" command
 * 
 * @param[in] inputList - A list of strings, the users input split by " "
 ******************************************************************************/
void InputProcessor::processPid(std::list<std::string> inputList)
{
    if (inputList.size() != 2)
    {
        std::cout << "  malformed command:\n    must be \"pid <command>\"\n";
        return;
    }

    for (std::string dir : fileProcessor.getSubDirectories("/proc/"))
    {
        std::string statusFile = "/proc/" + dir + "/status";
        if (fileProcessor.isFile(statusFile))
        {
            for (std::string s : fileProcessor.getFileContents(statusFile))
            {
                if (s.find("Name:") != std::string::npos &&
                    s.find(inputList.back()) != std::string::npos)
                {
                    std::cout << dir << "\n";
                }
            }
        }
    }
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process the "systat" command
 * 
 * @param[in] inputList - A list of strings, the users input split by " "
 ******************************************************************************/
void InputProcessor::processSystat(std::list<std::string> inputList)
{
    if (inputList.size() != 1)
    {
        std::cout << "  malformed command:\n    systat does not accept arguments\n";
        return;
    }

    std::cout << "\nversion:\n";
    for (std::string s : fileProcessor.getFileContents("/proc/version"))
    {
        std::cout << s << "\n";
    }

    std::cout << "\nuptime:\n";
    for (std::string s : fileProcessor.getFileContents("/proc/uptime"))
    {
        std::cout << s << "\n";
    }

    std::cout << "\nmeminfo:\n";
    for (std::string s : fileProcessor.getFileContents("/proc/meminfo"))
    {
        std::cout << s << "\n";
    }

    std::cout << "\ncpuinfo:\n";
    for (std::string s : fileProcessor.getFileContents("/proc/cpuinfo"))
    {
        if (s.find("vendor_id") != std::string::npos ||
            s.find("cpu family") != std::string::npos ||
            s.find("model") != std::string::npos ||
            s.find("model name") != std::string::npos ||
            s.find("stepping") != std::string::npos ||
            s.find("cpu MHz") != std::string::npos ||
            s.find("cache size") != std::string::npos)
        {
            std::cout << s << "\n";
        }
    }
    std::cout << "\n";
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process the "cd" command
 * 
 * @param[in] inputList - A list of strings, the users input split by " "
 ******************************************************************************/
void InputProcessor::processCd(std::list<std::string> inputList)
{
    if (inputList.size() != 2)
    {
        std::cout << "  malformed command:\n    must be \"cd <relative path>\" or \"cd <absolute path>\"\n";
        return;
    }

    if (!fileProcessor.isFolder(inputList.back()))
    {
        std::cout << inputList.back() << " is not a directory\n";
        return;
    }

    if (chdir(inputList.back().c_str()) == 0)
    {
        std::cout << "Changed to directory " << fileProcessor.getCurrentPath() << "\n";
    }
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This will process the "memman" command
 * 
 * @param[in] inputList - A list of strings, the users input split by " "
 ******************************************************************************/
void InputProcessor::memManager(std::list<std::string> inputList)
{
    int pageSize = 4096;

    if (inputList.size() != 2)
    {
        std::cout << "  malformed command:\n    must be \"memman <memory location>\"\n";
        return;
    }

    inputList.pop_front();
    if(!isInt(inputList.front()))
    {
        std::cout << "  malformed command:\n    <memory location> must be an integer\n";
        return;
    }

    int memoryLoc = stoi(inputList.front());

    std::cout << "The address " << memoryLoc << " contains:\n";
    std::cout << "page number = " << memoryLoc / pageSize << "\n";
    std::cout << "offset = " << memoryLoc % pageSize << "\n\n";
}


/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This is the function that will run in each thread of processTestpid
 * 
 * @param[in] obj_param - this is a pointer to the InputProcessor that created 
 *                        the thread
 * @return - required by pthread_create, always returns NULL
 ******************************************************************************/
void* ThreadEntryFunc(void *obj_param)
{
    InputProcessor *thr  = ((InputProcessor *)obj_param);
    
    int pid = thr->pidManager.allocatePid();
    std::cout << "allocated " << pid << "\n";

    usleep(rand()%500+1);

    thr->pidManager.releasePid(pid);
    std::cout << "releasing " << pid << "\n";

    return NULL;
}

/**************************************************************************/ /**
 * @author Dalton Baker
 * @par Description:
 * This processes the "testpid" command
 ******************************************************************************/
void InputProcessor::processTestpid()
{
    int numThreadsToMake = 100, error;
    pthread_t tid;
    pthread_t tids[numThreadsToMake];

    srand(0);

    pidManager.allocateMap();

    for (int i = 0; i < numThreadsToMake; i++)
    {
        error = pthread_create(&tid, NULL, ThreadEntryFunc, this);
        if (error != 0)
        {
            std::cout << "Thread creation failed: " << strerror(error) << "\n";
        }
        else
        {
            tids[i] = tid;
        }
    }

    for(pthread_t tidToJoin : tids)
    {
        pthread_join(tidToJoin, NULL);
    }
}